const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

app.get('/', (req, res) => {
  res.send('<p>Webhook Demo</p>');
  res.send('Express is working');
});

app.post('/webhook', jsonParser, (req, res) => {
  process_data(req.body);
  if (req.body.object_kind === 'build') {
    job_execution();
  } else if (req.body.object_kind === 'merge_request') {
    merge_request();
  }
  console.log('================================');
  console.log('Data received from GitLab is: ', req.body);

  // Change made during recording (pretty print).
  console.log(JSON.stringify(req.body, null, 4) + '\n');
  res.send('Data received!');
});

function process_data(json_data) {
  console.log('The data received is regarding: ${json_data.object_kind}');
  console.log('====================================');
}

function job_execution() {
  console.log('Do something with the job data');
  console.log('====================================');
}

function merge_request() {
  console.log('Do something with the merge request data');
  console.log('====================================');
}

app.listen(3000, () => console.log('Server running on port 3000'));

// In the above code, we first require the express module and create an instance of the Express application using express() method. We then require the body-parser module to parse the JSON data sent in the request body. We create a middleware using bodyParser.json() and pass it as a second argument to the app.post() method to parse the JSON data sent in the request body.

// We define two routes, one for the root path ('/') and another for the webhook endpoint ('/webhook'). In the root route, we simply send a response with some HTML content.

// In the webhook route, we check if the request method is POST and process the JSON data received by calling the process_data() function. We then check the value of object_kind in the JSON data and call the appropriate function based on the value. We then log the JSON data and send a response with the message "Data received!".

// Finally, we define the process_data(), job_execution(), and merge_request() functions that perform some actions based on the received data.

// Note that in Express, we need to explicitly start the server using the app.listen() method.